﻿using System;
using System.Collections.Generic;

namespace ApiProject.Entities
{
    public partial class Items
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
    }
}
