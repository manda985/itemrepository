﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ApiProject.Extensions
{
    public static class ItemExtensions
    {
        public static Items ToModel(this ApiProject.Entities.Items entity)
        {
            if(entity==null)
            {
                return null;
            }
            return new ApiProject.Items()
            {
                ID = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                Created = entity.Created
            };
        }
    }
}
