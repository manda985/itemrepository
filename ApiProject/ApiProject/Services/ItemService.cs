﻿using ApiProject.Entities;
using BergerNetCoreCommon.Factories;
using BergerNetCoreCommon.Models;
using BergerNetCoreCommon.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiProject.Extensions;

namespace ApiProject.Services
{
    public class ItemService:ServiceBase,IItemService
    {
        private DemoContext _context;

        public ItemService(DemoContext context) : base(new AuditContext(), "aaurl", 1)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Items>> GetAll()
        {
            IEnumerable<ApiProject.Items> result = null;
            using (var unitOfWork = this.ItemUnitOfWorkFactory.GenerateUnitOfWork())
            {
                var entities = await unitOfWork.GenericRepository.FindByExpressionAsync(i => i.Id > 0);
                if (entities !=null && entities.Any())
                {
                    result = entities.Select(i => i.ToModel());
                }
            }
            return result;
        }
        private IUnitOfWorkFactory<Entities.Items, Entities.DemoContext> _itemUnitOfWorkFactory;
        public IUnitOfWorkFactory<Entities.Items, Entities.DemoContext> ItemUnitOfWorkFactory
        {
            get
            {
                if(this._itemUnitOfWorkFactory==null)
                {
                    this._itemUnitOfWorkFactory = new UnitOfWorkFactory<Entities.Items, DemoContext>(_context);
                }
                return this._itemUnitOfWorkFactory;
            }
            set
            {
                this._itemUnitOfWorkFactory = value;
            }
        }


        

        




    }
}
